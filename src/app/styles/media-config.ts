export const breakpoints = {
  mobile: {
    value: '500',
    param: 'width'
  },
  tablet: {
    value: '620',
    param: 'width'
  },
  test1: {
    value: '750'
  },
  test2: {
    value: '960'
  },
  widescreen: {
    value: '1280',
    param: 'width'
  }
};
