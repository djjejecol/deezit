import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import { Actions, Effect } from '@ngrx/effects';
import * as TracksActions from '../actions/tracks.action';
import { environment } from '../../environments/environment';

@Injectable()
export class TracksEffects {
  constructor(private http: HttpClient, private actions$: Actions) {}
  @Effect()
  GetTracks: Observable<Action> = this.actions$
    .ofType<TracksActions.GetPlaylistTracks>(TracksActions.GET_PLAYLIST_TRACKS)
    .mergeMap(action =>
      this.http
        .get(`${environment.corsUrl}/${action.payload}`)
        .map(response => {
          return new TracksActions.GetPlaylistTracksSuccess(response);
        })
    );

  @Effect()
  AddTracks: Observable<Action> = this.actions$
    .ofType<TracksActions.AddPlaylistTracks>(TracksActions.ADD_PLAYLIST_TRACKS)
    .mergeMap(action =>
      this.http
        .get(`${environment.corsUrl}/${action.payload}`)
        .map(response => {
          return new TracksActions.AddPlaylistTracksSuccess(response);
        })
    );
}
