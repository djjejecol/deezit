import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import { Actions, Effect } from '@ngrx/effects';
import { environment } from '../../environments/environment';
import * as PlaylistsActions from '../actions/playlists.action';

@Injectable()
export class PlaylistsEffects {
  constructor(private http: HttpClient, private actions$: Actions) {}

  @Effect()
  GetPlaylists$: Observable<Action> = this.actions$
    .ofType<PlaylistsActions.FetchUserPlaylists>(
      PlaylistsActions.FETCH_USER_PLAYLISTS
    )
    .mergeMap(action =>
      this.http
        .get(`${environment.deezerApi.baseUrl}/user/${action.userId}/playlists`)
        .map((response: any) => {
          const playlists = response.data.map(item => item.id);
          const playlistPool = response.data.reduce((acc, playlist) => {
            acc[playlist.id] = playlist;
            return acc;
          }, {});
          const payload = { playlists, playlistPool };
          return new PlaylistsActions.FetchUserPlaylistsSuccess(payload);
        })
    );

  @Effect()
  GetPlaylist$: Observable<Action> = this.actions$
    .ofType<PlaylistsActions.FetchPlaylist>(PlaylistsActions.FETCH_PLAYLIST)
    .mergeMap(action =>
      this.http
        .get(`${environment.deezerApi.baseUrl}/playlist/${action.playlistId}`)
        .map((response: any) => {
          const selectedPlaylistId = response.id;
          const playlistPool = {
            [response.id]: response
          };
          const payload = { selectedPlaylistId, playlistPool };
          return new PlaylistsActions.FetchPlaylistSuccess(payload);
        })
    );
}
