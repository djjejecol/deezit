import { TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Store } from '@ngrx/store';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { AppComponent } from './app.component';
import { routes } from './routes';
import { SharedComponentsModule } from './shared-components/shared-components.module';
import { HomeModule } from './containers/home/home.module';
import { PageNotFoundModule } from './containers/pageNotFound/pageNotFound.module';
import { PlaylistDetailsModule } from './containers/playlist/playlist-details.module';

describe('AppComponent', () => {
  let location: Location;
  let router: Router;
  let store: Store<any>;
  let fixture;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [
        SharedComponentsModule,
        HomeModule,
        PageNotFoundModule,
        PlaylistDetailsModule,
        RouterTestingModule.withRoutes(routes)
      ],
      providers: [{ provide: Store }]
    }).compileComponents();
    router = TestBed.get(Router);
    router.initialNavigation();
    location = TestBed.get(Location);
    store = TestBed.get(Store);

    fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
  }));
  afterEach(() => {
    fixture.destroy();
  });
  it('should create the app', async(() => {
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it('fakeAsync works', fakeAsync(() => {
    const promise = new Promise(resolve => {
      setTimeout(resolve, 10);
    });
    let done = false;
    promise.then(() => (done = true));
    tick(50);
    expect(done).toBeTruthy();
  }));
  it('navigate to "', fakeAsync(() => {
    router.navigate(['']);
    tick();
    expect(location.path()).toBe('/');
  }));
});
