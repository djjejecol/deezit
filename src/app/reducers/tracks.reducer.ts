import {
  GET_PLAYLIST_TRACKS,
  GET_PLAYLIST_TRACKS_SUCCESS,
  ADD_PLAYLIST_TRACKS,
  ADD_PLAYLIST_TRACKS_SUCCESS
} from '../actions/tracks.action';

import * as TracksActions from '../actions/tracks.action';
import { Track } from '../models/track.model';

export type Action = TracksActions.All;

export interface State {
  tracks: Array<Track>;
  loading: boolean;
  total: number;
  next: string;
}

export const defaultState: any = {
  tracks: [],
  total: 0
};

const reducedActions = {
  [GET_PLAYLIST_TRACKS]: (state, action) => ({
    ...state,
    tracks: [],
    loading: true
  }),
  [GET_PLAYLIST_TRACKS_SUCCESS]: (state, action) => ({
    ...state,
    tracks: [...action.payload.data],
    loading: false,
    total: action.payload.total,
    next: action.payload.next
  }),
  [ADD_PLAYLIST_TRACKS_SUCCESS]: (state, action) => ({
    ...state,
    tracks: [...state.tracks, ...action.payload.data],
    loading: false,
    total: action.payload.total,
    next: action.payload.next
  })
};

// make sure a function is created (not a lambda => aot error)
export function reducer(state = defaultState, action: Action): State {
  if (!reducedActions.hasOwnProperty(action.type)) {
    return state;
  } else {
    return reducedActions[action.type](state, action);
  }
}

export const getTracks = (state: State) => state.tracks;
export const isLoading = (state: State) => state.loading;
