import { reducer, defaultState } from '../playlists.reducer';
import {
  FetchUserPlaylists,
  FetchUserPlaylistsSuccess,
  FetchPlaylist,
  FetchPlaylistSuccess
} from '../../actions/playlists.action';

describe('undefined action', () => {
  it('should return the default state', () => {
    const action = { type: 'NOOP' } as any;
    const result = reducer(defaultState, action);

    expect(result).toBe(defaultState);
  });
});
describe('playlist Reducer', () => {
  describe('[playlist] FetchUserPlaylists', () => {
    it('should toggle loading state', () => {
      const action = new FetchUserPlaylists(null);
      const result = reducer(defaultState, action);
      expect(result).toEqual({
        ...defaultState,
        loading: true
      });
    });
  });
  describe('[playlist] FetchUserPlaylistsSuccess', () => {
    it('should add playslits to default state', () => {
      const payload = {
        playlists: [1, 2],
        playlistPool: {
          1: 'test1',
          2: 'test2'
        }
      };
      const action = new FetchUserPlaylistsSuccess(payload);
      const result = reducer(defaultState, action);
      expect(result).toEqual({
        ...defaultState,
        ...payload
      });
    });
  });
  describe('[playlist] FetchPlaylist', () => {
    it('should toggle loading state', () => {
      const action = new FetchPlaylist(1);
      const result = reducer(defaultState, action);
      expect(result).toEqual({
        ...defaultState,
        loading: true
      });
    });
  });
  describe('[playlist] FetchPlaylistSuccess', () => {
    it('should add selectedPlaylistId & add playlistPool to default state', () => {
      const payload = {
        selectedPlaylistId: 1,
        playlistPool: {
          1: 'test1'
        }
      };
      const action = new FetchPlaylistSuccess(payload);
      const result = reducer(defaultState, action);
      expect(result).toEqual({
        ...defaultState,
        ...payload
      });
    });
  });
});
