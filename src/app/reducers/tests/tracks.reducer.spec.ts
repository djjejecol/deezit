import { reducer, defaultState } from '../tracks.reducer';
import {
  GetPlaylistTracks,
  GetPlaylistTracksSuccess
} from '../../actions/tracks.action';

describe('undefined action', () => {
  it('should return the default state', () => {
    const action = { type: 'NOOP' } as any;
    const result = reducer(defaultState, action);

    expect(result).toBe(defaultState);
  });
});
describe('tracks Reducer', () => {
  describe('[tracks] GetPlaylistTracks', () => {
    it('should toggle loading state', () => {
      const action = new GetPlaylistTracks(null);
      const result = reducer(defaultState, action);
      expect(result).toEqual({
        ...defaultState,
        loading: true
      });
    });
  });
  describe('[tracks] GetPlaylistTracksSuccess', () => {
    it('should add playslits to default state', () => {
      const payload = {
        data: [{ key: 'test1' }, { key: 'test2' }],
        total: 2
      };
      const action = new GetPlaylistTracksSuccess(payload);
      const result = reducer(defaultState, action);
      expect(result).toEqual({
        ...defaultState,
        tracks: payload.data,
        loading: false,
        total: payload.total
      });
    });
  });
});
