import { createSelector, ActionReducerMap } from '@ngrx/store';

import * as fromPlaylists from './playlists.reducer';
import * as fromTracks from './tracks.reducer';

export interface AppState {
  playlists: fromPlaylists.State;
  tracks: fromTracks.State;
}

export const reducers: ActionReducerMap<AppState> = {
  playlists: fromPlaylists.reducer,
  tracks: fromTracks.reducer
};

// Playlist state
export const selectPlaylistsState = createSelector(
  (state: AppState) => state.playlists
);

export const getPlaylists = createSelector(selectPlaylistsState, () => true);

// tracks state
export const selectTracksState = (state: AppState) => state.tracks;

export const getTracks = createSelector(
  selectTracksState,
  fromTracks.getTracks
);
