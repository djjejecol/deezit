import {
  FETCH_PLAYLIST,
  FETCH_PLAYLIST_SUCCESS,
  FETCH_USER_PLAYLISTS,
  FETCH_USER_PLAYLISTS_SUCCESS,
  SET_SELECTED_PLAYSLIT
} from '../actions/playlists.action';

import * as PlaylistActions from '../actions/playlists.action';
import { Playlist } from '../models/playlist.model';

export type Action = PlaylistActions.All;

export interface State {
  selectedPlaylistId: number;
  playlists: Array<number>;
  playlistPool: Object;
  loading: boolean;
}

export const defaultState: State = {
  selectedPlaylistId: null,
  playlists: [],
  playlistPool: {},
  loading: false
};

const reducedActions = {
  [FETCH_PLAYLIST]: (state, action) => ({
    ...state,
    loading: true
  }),
  [FETCH_USER_PLAYLISTS]: (state, action) => ({
    ...state,
    loading: true
  }),
  [FETCH_USER_PLAYLISTS_SUCCESS]: (state, action) => ({
    ...state,
    playlists: [...action.payload.playlists],
    playlistPool: { ...state.playlistPool, ...action.payload.playlistPool },
    loading: false
  }),
  [FETCH_PLAYLIST_SUCCESS]: (state, action) => ({
    ...state,
    selectedPlaylistId: action.payload.selectedPlaylistId,
    playlistPool: { ...state.playlistPool, ...action.payload.playlistPool },
    loading: false
  }),
  [SET_SELECTED_PLAYSLIT]: (state, action) => ({
    ...state,
    selectedPlaylistId: action.payload
  })
};

// make sure a function is created (not a lambda => aot error)
export function reducer(state: State = defaultState, action: Action): State {
  if (!reducedActions.hasOwnProperty(action.type)) {
    return state;
  } else {
    return reducedActions[action.type](state, action);
  }
}

export const getPlaylists = (state: State) => state.playlists;
export const getPlaylist = (state: State) => state.selectedPlaylistId;
export const isLoading = (state: State) => state.loading;
