import { Pipe, PipeTransform } from '@angular/core';

export const pad2 = (value: number) => {
  const leftDigit = value < 10 ? '0' : '';
  return `${leftDigit}${value}`;
};

@Pipe({
  name: 'minuteSeconds'
})
export class MinuteSecondsPipe implements PipeTransform {
  transform(value: number): string {
    let hours: number = null;
    let minutes: number = null;
    let seconds: number = value;
    let result: string;
    if (seconds >= 3600) {
      hours = Math.floor(seconds / 3600);
      seconds = seconds - hours * 3600;
      result = `${pad2(hours)}:`;
    }
    minutes = Math.floor(seconds / 60);
    seconds = seconds - minutes * 60;
    result = (result ? result : '') + `${pad2(minutes)}:${pad2(seconds)}`;
    return result;
  }
}
