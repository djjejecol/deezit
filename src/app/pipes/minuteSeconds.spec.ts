import { MinuteSecondsPipe, pad2 } from './minuteSeconds';

describe('pad2', () => {
  it('should return 2 digits', () => {
    expect(pad2(5)).toBe('05');
  });
  it('should return 2 digits', () => {
    expect(pad2(15)).toBe('15');
  });
});

describe('MinuteSecondsPipe', () => {
  let pipe: MinuteSecondsPipe;
  beforeEach(() => {
    pipe = new MinuteSecondsPipe();
  });
  it('0s', () => {
    expect(pipe.transform(0)).toBe('00:00');
  });
  it('10s', () => {
    expect(pipe.transform(10)).toBe('00:10');
  });
  it('60s', () => {
    expect(pipe.transform(60)).toBe('01:00');
  });
  it('90s', () => {
    expect(pipe.transform(90)).toBe('01:30');
  });
  it('600s', () => {
    expect(pipe.transform(600)).toBe('10:00');
  });
  it('3599s', () => {
    expect(pipe.transform(3599)).toBe('59:59');
  });
  it('3600s', () => {
    expect(pipe.transform(3600)).toBe('01:00:00');
  });
  it('4230s', () => {
    expect(pipe.transform(4230)).toBe('01:10:30');
  });
});
