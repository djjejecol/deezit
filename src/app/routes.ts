import { Routes } from '@angular/router';
import { AppComponent } from './app.component';

export const routes: Routes = [
  {
    path: '',
    loadChildren: './containers/home/home.module#HomeModule',
    pathMatch: 'full'
  },
  {
    path: 'playlist/:id',
    loadChildren:
      './containers/playlist/playlist-details.module#PlaylistDetailsModule'
  },
  {
    path: '**',
    loadChildren:
      './containers/pageNotFound/pageNotFound.module#PageNotFoundModule'
  }
];
