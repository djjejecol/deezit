import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-hamburger',
  templateUrl: './hamburger.component.html',
  styleUrls: ['./hamburger.component.css']
})
export class HamburgerComponent {
  @Output()
  onclick: EventEmitter<any> = new EventEmitter();

  handleClick(event: Event) {
    this.onclick.emit(event);
  }
}
