import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { HamburgerComponent } from './hamburger.component';
import { DebugElement } from '@angular/core';

describe('HamburgerComponent', () => {
  let fixture: ComponentFixture<HamburgerComponent>;
  let component: HamburgerComponent;
  let debugElement: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HamburgerComponent]
    }).compileComponents();
    fixture = TestBed.createComponent(HamburgerComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
  }));

  it('should create snapshots', () => {
    const app = debugElement.componentInstance;
    expect(fixture).toMatchSnapshot();
  });

  it('should', async(() => {
    let click: string = null;
    const button = debugElement.query(By.css('svg'));
    component.onclick.subscribe(value => (click = value));
    button.triggerEventHandler('click', 'resultEvent');
    expect(click).toBe('resultEvent');
  }));
});
