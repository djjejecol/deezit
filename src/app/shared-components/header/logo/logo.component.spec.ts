import { TestBed, async } from '@angular/core/testing';
import { LogoComponent } from './logo.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('LogoComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LogoComponent],
      imports: [RouterTestingModule]
    }).compileComponents();
  }));
  it('should create snapshots', () => {
    const fixture = TestBed.createComponent(LogoComponent);
    const app = fixture.debugElement.componentInstance;
    expect(fixture).toMatchSnapshot();
  });
});
