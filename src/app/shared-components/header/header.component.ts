import { Component, OnInit } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
import { breakpoints } from '../../styles/media-config';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  animations: [
    trigger('slideInOut', [
      state('in', style({ width: '75%' })),
      state('out', style({ width: '0' })),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ])
  ]
})
export class HeaderComponent implements OnInit {
  mobile = false;
  state = 'out';
  routes = [
    { url: '/contact', name: 'contact' },
    { url: '/about', name: 'about' }
  ];
  toggleMenu(event: Event) {
    this.state = this.state === 'out' ? 'in' : 'out';
  }
  ngOnInit() {
    this.compute(window.innerWidth);
  }
  onResize(event) {
    this.compute(event.target.innerWidth);
  }
  compute(width) {
    this.mobile = width < breakpoints.tablet.value;
  }
}
