import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  HeaderComponent,
  HamburgerComponent,
  LogoComponent,
  MenuItemComponent
} from './index';

@NgModule({
  imports: [CommonModule, BrowserAnimationsModule, RouterModule],
  declarations: [
    HeaderComponent,
    HamburgerComponent,
    LogoComponent,
    MenuItemComponent
  ],
  exports: [HeaderComponent]
})
export class SharedComponentsModule {}
