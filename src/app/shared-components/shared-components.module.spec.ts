import { SharedComponentsModule } from './shared-components.module';

describe('PlaylistModule', () => {
  let module: SharedComponentsModule;

  beforeEach(() => {
    module = new SharedComponentsModule();
  });

  it('should create an instance', () => {
    expect(module).toBeTruthy();
  });
});
