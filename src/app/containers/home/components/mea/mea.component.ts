import { Component, OnInit } from '@angular/core';
import { breakpoints } from '../../../../styles/media-config';

@Component({
  selector: 'app-mea',
  templateUrl: './mea.component.html',
  styleUrls: ['./mea.component.css']
})
export class MEAComponent implements OnInit {
  showMEA = true;
  ngOnInit() {
    this.computeShowMEA(window.innerWidth);
  }
  onResize(event) {
    this.computeShowMEA(event.target.innerWidth);
  }
  computeShowMEA(width) {
    this.showMEA = width < breakpoints.tablet.value;
  }
}
