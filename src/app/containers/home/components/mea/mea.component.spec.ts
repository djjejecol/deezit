import { TestBed, async } from '@angular/core/testing';
import { MEAComponent } from './mea.component';

describe('MEAComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MEAComponent]
    }).compileComponents();
  }));
  it('should create snapshots', () => {
    const fixture = TestBed.createComponent(MEAComponent);
    const app = fixture.debugElement.componentInstance;
    expect(fixture).toMatchSnapshot();
  });
});
