import { Component, Input } from '@angular/core';
import { Playlist } from '../../../../models/playlist.model';

@Component({
  selector: 'app-playlists',
  templateUrl: './playlists.component.html',
  styleUrls: ['./playlists.component.css']
})
export class PlaylistsComponent {
  @Input()
  title = 'title';
  @Input()
  items: Array<Playlist> = [];
}
