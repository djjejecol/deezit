import { TestBed, async } from '@angular/core/testing';
import { PlaylistsComponent } from './playlists.component';
import { PlaylistComponent } from './playlist/playlist.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('PlaylistsComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlaylistsComponent, PlaylistComponent],
      imports: [RouterTestingModule]
    }).compileComponents();
  }));
  it('should create snapshots', () => {
    const fixture = TestBed.createComponent(PlaylistsComponent);
    const app = fixture.debugElement.componentInstance;
    expect(fixture).toMatchSnapshot();
  });
});
