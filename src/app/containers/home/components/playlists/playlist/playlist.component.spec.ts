import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { PlaylistComponent } from './playlist.component';

describe('PlaylistComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlaylistComponent],
      imports: [RouterTestingModule]
    }).compileComponents();
  }));
  it('should create snapshots', () => {
    const fixture = TestBed.createComponent(PlaylistComponent);
    const app = fixture.debugElement.componentInstance;
    expect(fixture).toMatchSnapshot();
  });
});
