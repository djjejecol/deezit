import { Component, Input, OnInit } from '@angular/core';
import { Playlist } from './../../../../../models/playlist.model';
import { breakpoints } from '../../../../../styles/media-config';

@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.css']
})
export class PlaylistComponent implements OnInit {
  @Input()
  data: Playlist = null;

  mobile = true;
  ngOnInit() {
    this.computeShowTitle(window.innerWidth);
  }
  onResize(event) {
    this.computeShowTitle(event.target.innerWidth);
  }
  computeShowTitle(width) {
    this.mobile = width < breakpoints.test2.value;
  }
}
