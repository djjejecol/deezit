import { TestBed, async } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { HomeComponent } from './home.component';
import { MEAComponent } from './components/mea/mea.component';
import { PlaylistsComponent } from './components/playlists/playlists.component';
import { PlaylistComponent } from './components/playlists/playlist/playlist.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('HomeComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HomeComponent,
        MEAComponent,
        PlaylistsComponent,
        PlaylistComponent
      ],
      providers: [{ provide: Store }],
      imports: [RouterTestingModule]
    }).compileComponents();
  }));
  it('should create snapshots', () => {
    const fixture = TestBed.createComponent(HomeComponent);
    const app = fixture.debugElement.componentInstance;
    expect(fixture).toMatchSnapshot();
  });
});
