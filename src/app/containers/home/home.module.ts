import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';

import {
  HomeComponent,
  MEAComponent,
  PlaylistsComponent,
  PlaylistComponent
} from './index';

@NgModule({
  imports: [CommonModule, HomeRoutingModule],
  declarations: [
    HomeComponent,
    MEAComponent,
    PlaylistsComponent,
    PlaylistComponent
  ],
  exports: [HomeComponent]
})
export class HomeModule {}
