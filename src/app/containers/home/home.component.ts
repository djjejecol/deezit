import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { AppState } from '../../reducers';
import { Playlist } from '../../models/playlist.model';
import { FetchUserPlaylists } from '../../actions/playlists.action';
import 'rxjs/add/operator/zip';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  constructor(private store: Store<AppState>) {}
  title = 'Sélection de playlists';
  userId = 2529;
  ids$: Observable<number[]>;
  pool$: Observable<Object>;
  playlists$: Observable<Playlist[]>;
  private sub: any;

  ngOnInit() {
    this.ids$ = this.store.select(state => state.playlists.playlists);
    this.pool$ = this.store.select(state => state.playlists.playlistPool);

    this.playlists$ = this.ids$.zip(this.pool$, (ids, values) =>
      ids.map(id => values[id])
    );
    this.sub = this.playlists$.subscribe(playlists => {
      if (!playlists.length) {
        this.store.dispatch(new FetchUserPlaylists(this.userId));
      }
    });
  }
  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }
}
