import { TestBed, async } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import {
  PlaylistDetailsComponent,
  MediaInfosComponent,
  TracksComponent,
  TrackItemComponent,
  MediaBoardComponent
} from './index';

import { MinuteSecondsPipe } from '../../pipes/minuteSeconds';

describe('PlaylistDetailsComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PlaylistDetailsComponent,
        MediaInfosComponent,
        TracksComponent,
        TrackItemComponent,
        MediaBoardComponent,
        MinuteSecondsPipe
      ],
      providers: [{ provide: Store }, { provide: ActivatedRoute }],
      imports: [InfiniteScrollModule, RouterTestingModule]
    }).compileComponents();
  }));
  it('should create snapshots', () => {
    const fixture = TestBed.createComponent(PlaylistDetailsComponent);
    const app = fixture.debugElement.componentInstance;
    expect(fixture).toMatchSnapshot();
  });
});
