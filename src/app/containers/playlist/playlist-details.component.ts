import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { AppState } from '../../reducers';
import { Playlist } from '../../models/playlist.model';
import {
  FetchPlaylist,
  SetSelectedPlaylist
} from '../../actions/playlists.action';
import {
  GetPlaylistTracks,
  AddPlaylistTracks
} from '../../actions/tracks.action';
import { Track } from '../../models/track.model';
import 'rxjs/add/operator/combineLatest';

@Component({
  selector: 'app-playlist-details',
  templateUrl: './playlist-details.component.html',
  styleUrls: ['./playlist-details.component.css']
})
export class PlaylistDetailsComponent implements OnInit, OnDestroy {
  id: number;
  private subs: any = [];
  id$: Observable<number>;
  playlistPool$: Observable<Object>;
  playlist$: Observable<Playlist>;
  tracks$: Observable<Track[]>;
  next$: Observable<string>;
  next: string;

  constructor(private route: ActivatedRoute, private store: Store<AppState>) {}

  ngOnInit() {
    this.id$ = this.store.select(state => state.playlists.selectedPlaylistId);
    this.playlistPool$ = this.store.select(
      state => state.playlists.playlistPool
    );

    this.playlist$ = this.id$.combineLatest(this.playlistPool$, (id, pool) => {
      const playlist = pool[id];
      if (!playlist) {
        this.store.dispatch(new FetchPlaylist(this.id));
      } else {
        this.store.dispatch(new GetPlaylistTracks(playlist.tracklist));
      }
      return playlist;
    });

    this.tracks$ = this.store.select(state => state.tracks.tracks);
    this.next$ = this.store.select(state => state.tracks.next);

    this.subs.push(
      this.route.params.subscribe(params => {
        this.id = +params['id']; // (+) converts string 'id' to a number
        this.store.dispatch(new SetSelectedPlaylist(this.id));
      })
    );

    this.subs.push(this.next$.subscribe(value => (this.next = value)));
  }

  onScroll() {
    if (this.next) {
      this.store.dispatch(new AddPlaylistTracks(this.next));
    }
  }

  ngOnDestroy() {
    this.subs.forEach(sub => {
      if (sub) {
        sub.unsubscribe();
      }
    });
  }
}
