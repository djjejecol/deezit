import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlaylistDetailsRoutingModule } from './playlist-details-routing.module';
import { MinuteSecondsPipe } from '../../pipes/minuteSeconds';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import {
  PlaylistDetailsComponent,
  MediaInfosComponent,
  TracksComponent,
  TrackItemComponent,
  MediaBoardComponent
} from './index';

@NgModule({
  imports: [CommonModule, InfiniteScrollModule, PlaylistDetailsRoutingModule],
  declarations: [
    PlaylistDetailsComponent,
    MediaInfosComponent,
    TracksComponent,
    TrackItemComponent,
    MediaBoardComponent,
    MinuteSecondsPipe
  ],
  exports: [PlaylistDetailsComponent]
})
export class PlaylistDetailsModule {}
