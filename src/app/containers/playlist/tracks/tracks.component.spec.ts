import { TestBed, async } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { TracksComponent } from './tracks.component';
import { TrackItemComponent } from './track-item/track-item.component';
import { MinuteSecondsPipe } from '../../../pipes/minuteSeconds';

describe('TracksComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TracksComponent, TrackItemComponent, MinuteSecondsPipe],
      providers: [{ provide: Store }]
    }).compileComponents();
  }));
  it('should create snapshots', () => {
    const fixture = TestBed.createComponent(TracksComponent);
    const app = fixture.debugElement.componentInstance;
    expect(fixture).toMatchSnapshot();
  });
});
