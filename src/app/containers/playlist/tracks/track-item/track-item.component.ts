import { Component, Input } from '@angular/core';
import { Track } from '../../../../models/track.model';

@Component({
  selector: 'app-track-item',
  templateUrl: './track-item.component.html',
  styleUrls: ['./track-item.component.css']
})
export class TrackItemComponent {
  @Input()
  position: number = null;
  @Input()
  title: String = null;
  @Input()
  artist: String = null;
  @Input()
  duration: number = null;
}
