import { TestBed, async } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { TrackItemComponent } from './track-item.component';
import { MinuteSecondsPipe } from '../../../../pipes/minuteSeconds';

describe('TrackItemComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TrackItemComponent, MinuteSecondsPipe],
      providers: [{ provide: Store }]
    }).compileComponents();
  }));
  it('should create snapshots', () => {
    const fixture = TestBed.createComponent(TrackItemComponent);
    const app = fixture.debugElement.componentInstance;
    expect(fixture).toMatchSnapshot();
  });
});
