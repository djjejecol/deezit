import { PlaylistDetailsModule } from './playlist-details.module';

describe('PlaylistModule', () => {
  let playlistModule: PlaylistDetailsModule;

  beforeEach(() => {
    playlistModule = new PlaylistDetailsModule();
  });

  it('should create an instance', () => {
    expect(playlistModule).toBeTruthy();
  });
});
