import { TestBed, async } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { MediaBoardComponent } from './media-board.component';
import { MinuteSecondsPipe } from '../../../../pipes/minuteSeconds';

describe('HomeComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MediaBoardComponent, MinuteSecondsPipe],
      providers: [{ provide: Store }]
    }).compileComponents();
  }));
  it('should create snapshots', () => {
    const fixture = TestBed.createComponent(MediaBoardComponent);
    const app = fixture.debugElement.componentInstance;
    expect(fixture).toMatchSnapshot();
  });
});
