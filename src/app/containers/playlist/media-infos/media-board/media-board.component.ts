import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-media-board',
  templateUrl: './media-board.component.html',
  styleUrls: ['./media-board.component.css']
})
export class MediaBoardComponent {
  @Input()
  title: String = null;
  @Input()
  creationDate: Date = null;
  @Input()
  nbTracks: number = null;
  @Input()
  duration: number = null;
  @Input()
  creator: any = null;
}
