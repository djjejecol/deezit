import { TestBed, async } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { MediaInfosComponent } from './media-infos.component';
import { MediaBoardComponent } from './media-board/media-board.component';
import { MinuteSecondsPipe } from '../../../pipes/minuteSeconds';

describe('HomeComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MediaInfosComponent,
        MediaBoardComponent,
        MinuteSecondsPipe
      ],
      providers: [{ provide: Store }]
    }).compileComponents();
  }));
  it('should create snapshots', () => {
    const fixture = TestBed.createComponent(MediaInfosComponent);
    const app = fixture.debugElement.componentInstance;
    expect(fixture).toMatchSnapshot();
  });
});
