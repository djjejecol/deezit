import { Component, Input } from '@angular/core';
import { Url } from 'url';

@Component({
  selector: 'app-media-infos',
  templateUrl: './media-infos.component.html',
  styleUrls: ['./media-infos.component.css']
})
export class MediaInfosComponent {
  @Input()
  title: String = null;
  @Input()
  creationDate: Date = null;
  @Input()
  nbTracks: number = null;
  @Input()
  duration: number = null;
  @Input()
  creator: Object = null;
  @Input()
  cover: Url = null;
}
