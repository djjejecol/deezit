import { TestBed, async } from '@angular/core/testing';
import { PageNotFoundComponent } from './pageNotFound.component';

describe('PageNotFoundComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PageNotFoundComponent]
    }).compileComponents();
  }));
  it('should create snapshots', () => {
    const fixture = TestBed.createComponent(PageNotFoundComponent);
    const app = fixture.debugElement.componentInstance;
    expect(fixture).toMatchSnapshot();
  });
});
