import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { routes } from './routes';
import { reducers } from './reducers';
import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { PlaylistsEffects } from './effects/playlists.effect';
import { TracksEffects } from './effects/tracks.effect';
import { SharedComponentsModule } from './shared-components/shared-components.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    SharedComponentsModule,
    StoreModule.forRoot(reducers),
    ...environment.imports,
    EffectsModule.forRoot([PlaylistsEffects, TracksEffects]),
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
