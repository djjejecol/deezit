import { Url } from 'url';

export interface Playlist {
  id: number;
  picture: Url;
  picture_small: Url;
  picture_medium: Url;
  picture_big: Url;
  picture_xl: Url;
  tracklist: Url;
  title: string;
}
