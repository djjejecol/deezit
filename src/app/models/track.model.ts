export interface Track {
  id: number;
  duration: number;
  title: string;
}
