import { Action } from '@ngrx/store';
import { Url } from 'url';

export const GET_PLAYLIST_TRACKS = 'deezerApi/GET_PLAYLIST_TRACKS';
export const GET_PLAYLIST_TRACKS_SUCCESS =
  'deezerApi/GET_PLAYLIST_TRACKS_SUCCESS';
export const ADD_PLAYLIST_TRACKS = 'deezerApi/ADD_PLAYLIST_TRACKS';
export const ADD_PLAYLIST_TRACKS_SUCCESS =
  'deezerApi/ADD_PLAYLIST_TRACKS_SUCCESS';

export class GetPlaylistTracks implements Action {
  readonly type = GET_PLAYLIST_TRACKS;
  constructor(public payload: string) {}
}

export class GetPlaylistTracksSuccess implements Action {
  readonly type = GET_PLAYLIST_TRACKS_SUCCESS;
  constructor(public payload: any) {}
}

export class AddPlaylistTracks implements Action {
  readonly type = ADD_PLAYLIST_TRACKS;
  constructor(public payload: string) {}
}

export class AddPlaylistTracksSuccess implements Action {
  readonly type = ADD_PLAYLIST_TRACKS_SUCCESS;
  constructor(public payload: any) {}
}

export type All =
  | GetPlaylistTracks
  | GetPlaylistTracksSuccess
  | AddPlaylistTracks
  | AddPlaylistTracksSuccess;
