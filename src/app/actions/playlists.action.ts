import { Action } from '@ngrx/store';

export const FETCH_USER_PLAYLISTS = 'deezerApi/FETCH_USER_PLAYLISTS';
export const FETCH_USER_PLAYLISTS_SUCCESS =
  'deezerApi/FETCH_USER_PLAYLISTS_SUCCESS';
export const FETCH_PLAYLIST = 'deezerApi/FETCH_PLAYLIST';
export const FETCH_PLAYLIST_SUCCESS = 'deezerApi/FETCH_PLAYLIST_SUCCESS';

export const SET_SELECTED_PLAYSLIT = 'deezerApi/SET_SELECTED_PLAYLIST';

export class FetchUserPlaylists implements Action {
  readonly type = FETCH_USER_PLAYLISTS;
  constructor(public userId: number) {}
}

export class FetchUserPlaylistsSuccess implements Action {
  readonly type = FETCH_USER_PLAYLISTS_SUCCESS;
  constructor(public payload: any) {}
}

export class FetchPlaylist implements Action {
  readonly type = FETCH_PLAYLIST;
  constructor(public playlistId: number) {}
}

export class FetchPlaylistSuccess implements Action {
  readonly type = FETCH_PLAYLIST_SUCCESS;
  constructor(public payload: any) {}
}

export class SetSelectedPlaylist implements Action {
  readonly type = SET_SELECTED_PLAYSLIT;
  constructor(public payload: number) {}
}

export type All =
  | FetchUserPlaylists
  | FetchUserPlaylistsSuccess
  | FetchPlaylist
  | FetchPlaylistSuccess
  | SetSelectedPlaylist;
