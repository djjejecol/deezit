export const environment = {
  production: true,
  deezerApi: {
    //trick to simulate cors
    baseUrl: 'https://cors-anywhere.herokuapp.com/https://api.deezer.com'
  },
  corsUrl: 'https://cors-anywhere.herokuapp.com',
  imports: []
};
