# Deezit

[![build status](https://gitlab.com/djjejecol/deezit/badges/develop/build.svg)](https://gitlab.com/djjejecol/commits/develop)

[![coverage report](https://gitlab.com/djjejecol/deezit/badges/develop/coverage.svg)](https://gitlab.com/djjejecol/commits/develop)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.3.

## Gitlab

### Branches

2 branches have been created:

- a develop branch used to merge any of a feature branch
- a master branch used for production

### Pipelines

3 pipelines are played on the develop and master branches. The pipelines are:

- test: unit tests are played and test coverage is computed
- build: the application is built
- deploy: the application is deployed to https://deezit-dev.surge.sh for the develop branch and to https://deezit.surge.sh

Note that the last pipeline is not played on features branch.

### TSLint & Pettier

- TSlint and prettier are played on pre-commit
- Tests are run on pre-push

### Gitlab tasks board

I used Gitlab to organize and manage the project. Issues can be a :

- feature
- enhancement
- bug

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `npm run test` to execute the unit tests via [Jest](https://jestjs.io/).
